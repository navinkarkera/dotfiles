#!/bin/zsh

# profile file. Runs on login. Environmental variables are set here.

# If you don't plan on reverting to bash, you can remove the link in ~/.profile
# to clean up.

# Adds `~/.local/bin` to $PATH
export PATH="$HOME/.local/bin:$HOME/.local/share/cargo/bin:$PATH"

unsetopt PROMPT_SP

# Default programs:
if [[ "$(command -v nvim)" ]]; then
    export EDITOR='nvim'
fi
export TERMINAL="wezterm"
export BROWSER="firefox"
export GPG_TTY=$(tty)
export MANPAGER='bat'
export MANWIDTH=999

# ~/ Clean-up:
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
# export XINITRC="${XDG_CONFIG_HOME:-$HOME/.config}/x11/xinitrc"
#export XAUTHORITY="$XDG_RUNTIME_DIR/Xauthority" # This line will break some DMs.
# export NOTMUCH_CONFIG="${XDG_CONFIG_HOME:-$HOME/.config}/notmuch-config"
# export GTK2_RC_FILES="${XDG_CONFIG_HOME:-$HOME/.config}/gtk-2.0/gtkrc-2.0"
export LESSHISTFILE="-"
export WGETRC="${XDG_CONFIG_HOME:-$HOME/.config}/wget/wgetrc"
export INPUTRC="${XDG_CONFIG_HOME:-$HOME/.config}/shell/inputrc"
export ZDOTDIR="${XDG_CONFIG_HOME:-$HOME/.config}/zsh"
#export ALSA_CONFIG_PATH="$XDG_CONFIG_HOME/alsa/asoundrc"
export GNUPGHOME="${XDG_DATA_HOME:-$HOME/.local/share}/gnupg"
# export WINEPREFIX="${XDG_DATA_HOME:-$HOME/.local/share}/wineprefixes/default"
# export KODI_DATA="${XDG_DATA_HOME:-$HOME/.local/share}/kodi"
export PASSWORD_STORE_DIR="${XDG_DATA_HOME:-$HOME/.local/share}/password-store"
export TMUX_TMPDIR="$XDG_RUNTIME_DIR"
# export ANDROID_SDK_HOME="${XDG_CONFIG_HOME:-$HOME/.config}/android"
export CARGO_HOME="${XDG_DATA_HOME:-$HOME/.local/share}/cargo"
export GOPATH="${XDG_DATA_HOME:-$HOME/.local/share}/go"
export ANSIBLE_CONFIG="${XDG_CONFIG_HOME:-$HOME/.config}/ansible/ansible.cfg"
export UNISON="${XDG_DATA_HOME:-$HOME/.local/share}/unison"
export HISTFILE="${XDG_DATA_HOME:-$HOME/.local/share}/history"
# export WEECHAT_HOME="${XDG_CONFIG_HOME:-$HOME/.config}/weechat"
# export MBSYNCRC="${XDG_CONFIG_HOME:-$HOME/.config}/mbsync/config"
# export ELECTRUMDIR="${XDG_DATA_HOME:-$HOME/.local/share}/electrum"
export TASKRC="${XDG_CONFIG_HOME:-$HOME/.config}/task/taskrc"
export TMUXIFIER_LAYOUT_PATH="$HOME/dotfiles/tmux-layouts"

export PATH="$GOPATH/bin:$PATH"

# Python pyenv
export PYENV_ROOT="$HOME/.pyenv"

# Other program settings:
export BAT_THEME="ansi"
export ZK_NOTEBOOK_DIR="/home/navin/Documents/notes"
export YDOTOOL_SOCKET="/tmp/.ydotool_socket"
export NB_DIR="/home/navin/Documents/notes-nb"
export TS_ONFINISH="/home/navin/.local/bin/tsp-ring"
export DICS="/usr/share/stardict/dic/"
export SUDO_ASKPASS="$HOME/.local/bin/dmenupass"
# export FZF_DEFAULT_OPTS="--ansi --layout=reverse"
# export FZF_DEFAULT_OPTS=" \
# --color=bg+:#1f1f28,bg:#1e1e2e,spinner:#f5e0dc,hl:#f38ba8 \
# --color=fg:#dcd7ba,header:#f38ba8,info:#cba6f7,pointer:#f5e0dc \
# --color=marker:#f5e0dc,fg+:#cdd6f4,prompt:#cba6f7,hl+:#f38ba8"
export FZF_DEFAULT_COMMAND="fd --type file -E .git --follow -H"
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND . $HOME"
export FZF_CTRL_T_OPTS="--preview 'bat --style=numbers --color=always --line-range :100 {}'"
# _gen_fzf_default_opts() {
#   # local base03="234"
#   # local base02="235"
#   # local base01="240"
#   # local base00="241"
#   # local base0="244"
#   # local base1="245"
#   # local base2="254"
#   # local base3="230"
#   # local yellow="136"
#   # local orange="166"
#   # local red="160"
#   # local magenta="125"
#   # local violet="61"
#   # local blue="33"
#   # local cyan="37"
#   # local green="64"
#   # Uncomment for truecolor, if your terminal supports it.
#   local base03="#002b36"
#   local base02="#073642"
#   local base01="#586e75"
#   local base00="#657b83"
#   local base0="#839496"
#   local base1="#93a1a1"
#   local base2="#eee8d5"
#   local base3="#fdf6e3"
#   local yellow="#b58900"
#   local orange="#cb4b16"
#   local red="#dc322f"
#   local magenta="#d33682"
#   local violet="#6c71c4"
#   local blue="#268bd2"
#   local cyan="#2aa198"
#   local green="#859900"
#
#   # Comment and uncomment below for the light theme.
#
#   # Solarized Dark color scheme for fzf
#   #export FZF_DEFAULT_OPTS=$FZF_DEFAULT_OPTS"
#   #  --color fg:-1,bg:-1,hl:$blue,fg+:$base2,bg+:$base02,hl+:$blue
#   #  --color info:$yellow,prompt:$yellow,pointer:$base3,marker:$base3,spinner:$yellow
#   #"
#   ## Solarized Light color scheme for fzf
#   export FZF_DEFAULT_OPTS=$FZF_DEFAULT_OPTS"
#     --color fg:-1,hl:$blue,fg+:$base02,bg+:$base2,hl+:$blue
#     --color info:$yellow,prompt:$yellow,pointer:$base03,marker:$base03,spinner:$yellow
#   "
# }
# _gen_fzf_default_opts
export FZF_ALT_C_COMMAND="fd --type d --color=auto . $HOME"
export FZF_ALT_C_OPTS="--preview 'exa --tree --level=3 {}'"
export FZF_TMUX_HEIGHT=80%
export FZF_TMUX_OPTS="-p"
export FZF_CTRL_R_OPTS="--reverse --preview 'echo {}' --preview-window down:3:hidden:wrap --bind '?:toggle-preview'"
export LESS=-R
export LESS_TERMCAP_mb="$(printf '%b' '[1;31m')"
export LESS_TERMCAP_md="$(printf '%b' '[1;36m')"
export LESS_TERMCAP_me="$(printf '%b' '[0m')"
export LESS_TERMCAP_so="$(printf '%b' '[01;44;33m')"
export LESS_TERMCAP_se="$(printf '%b' '[0m')"
export LESS_TERMCAP_us="$(printf '%b' '[1;32m')"
export LESS_TERMCAP_ue="$(printf '%b' '[0m')"
export LESSOPEN="| /usr/bin/highlight -O ansi %s 2>/dev/null"
export QT_QPA_PLATFORMTHEME="gtk2"	# Have QT use gtk2 theme.
export MOZ_USE_XINPUT2="1"		# Mozilla smooth scrolling/touchpads.
export AWT_TOOLKIT="MToolkit wmname LG3D"	#May have to install wmname
export _JAVA_AWT_WM_NONREPARENTING=1	# Fix for Java applications in dwm
# export CARGO_TARGET_DIR="/home/navin/.local/share/shared_cargo_target/"
export NVIM_SERVER="/tmp/default-nvim-server.pipe"
export TERMV_DEFAULT_MPV_FLAGS="--no-vid"

[ ! -f ${XDG_CONFIG_HOME:-$HOME/.config}/shell/shortcutrc ] && shortcuts >/dev/null 2>&1 &

# Start graphical server on user's current tty if not already running.
[ "$(tty)" = "/dev/tty1" ] && ! pidof -s Xorg >/dev/null 2>&1 && exec startx "$XINITRC"

# Switch escape and caps if tty and no passwd required:
# sudo -n loadkeys ${XDG_DATA_HOME:-$HOME/.local/share}/larbs/ttymaps.kmap 2>/dev/null
